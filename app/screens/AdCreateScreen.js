import React from "react";
import { SafeAreaView, ScrollView, TextInput, StyleSheet, Alert, Platform } from 'react-native';
import FlatButton from "../components/button";
import * as ImagePicker from 'expo-image-picker';
import fetch from "node-fetch";

export default class AdCreateScreen extends React.Component {
    constructor (props) {
        super(props);
        this.state = {
            title: '',
            description: '',
            contact: '',
            image: '',
            canValidate: false
        }
        this.onChooseImage = this.onChooseImage.bind(this)
        this.onChangeTitle = this.onChangeTitle.bind(this)
        this.onChangeDescription = this.onChangeDescription.bind(this)
        this.onChangeContact = this.onChangeContact.bind(this)
        this.onValidate = this.onValidate.bind(this)
    }

    onChangeTitle (title) {
        this.setState({ title })
        this.checkValidState()
    }

    onChangeDescription (description) {
        this.setState({ description })
        this.checkValidState()
    }

    onChangeContact (contact) {
        this.setState({ contact })
        this.checkValidState()
    }

    onChooseImage () {
        (async () => {
            if (Platform.OS !== 'web') {
                const { status } = await ImagePicker.requestMediaLibraryPermissionsAsync();
                if (status !== 'granted') {
                    Alert.alert('Ereur', 'La permission d\'accès aux images est nécessaire pour continuer.')
                    return
                }
            }
            let response =  await ImagePicker.launchImageLibraryAsync({
                mediaTypes: ImagePicker.MediaTypeOptions.Images,
                allowsEditing: true,
                quality: 1,
                base64: true,
                allowsMultipleSelection: false,
                aspect: [16, 9],
                exif: false
            })
            if (response.cancelled) {
                Alert.alert(
                    'Erreur',
                    'Vous n\'avez pas sélectionné d\'image',
                    [
                        {
                            text: 'Annuler',
                            style: 'cancel'
                        },
                        {
                            text: 'Réessayer',
                            style: 'default',
                            onPress: this.onChooseImage
                        }
                    ]
                );
                return;
            }
            const buf = response.uri.split('.')
            const type = buf[buf.length - 1]
            this.setState({ image: `data:image/${type};base64,${response.base64}` })
            this.checkValidState()
        })()
    }

    checkValidState () {
        if (
            this.state.contact.length === 0
            || this.state.description.length === 0
            || this.state.image.length === 0
            || this.state.title.length === 0
        ) {
            this.setState({ canValidate: false })
        } else {
            this.setState({ canValidate: true })
        }
    }

    onValidate () {
        console.log(this.state.title)
        fetch('http://node02.waytostars.fr:43017/advert', {
            method: 'POST',
            headers: { 'Content-Type': 'application/json' },
            body: JSON.stringify({
                title: this.state.title,
                description: this.state.description,
                contact: this.state.contact,
                image: this.state.image
            })
        })
        .then((res) => {
            console.log(res)
            this.props.navigation.goBack()
        }).catch(err => { 
            console.log(err)
        })
    }

    render () {
        return (
            <SafeAreaView>
                <ScrollView>
                <TextInput
                    style={styles.input}
                    onChangeText={this.onChangeTitle}
                    value={this.state.title}
                    placeholder="Insérez un titre"
                    placeholderTextColor="#000000a0"
                    editable={true}
                />
                <TextInput
                    style={styles.inputMultiline}
                    onChangeText={this.onChangeDescription}
                    value={this.state.description}
                    multiline={true}
                    editable={true}
                    numberOfLines={10}
                    placeholder="Insérez une description"
                    placeholderTextColor="#000000a0"
                />
                <TextInput
                    style={styles.input}
                    onChangeText={this.onChangeContact}
                    value={this.state.contact}
                    editable={true}
                    placeholder="Où peut-on vous contacter ?"
                    placeholderTextColor="#000000a0"
                />
                <FlatButton
                    text="Choisir une image"
                    onPress={this.onChooseImage}
                />
                <FlatButton
                    text="Valider"
                    onPress={this.onValidate}
                    disabled={!this.state.canValidate}
                />
                </ScrollView>
            </SafeAreaView>
        )
    }
}

const styles = StyleSheet.create({
    input: {
        minHeight: 40,
        margin: 12,
        padding: 12,
        borderWidth: 1,
        borderRadius: 5,
        backgroundColor: "#fff"
    },
    inputMultiline: {
        minHeight: 200,
        margin: 12,
        padding: 12,
        borderWidth: 1,
        borderRadius: 5,
        backgroundColor: "#fff"
    },
});