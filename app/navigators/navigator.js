import { NavigationContainer } from "@react-navigation/native";
import { createStackNavigator } from '@react-navigation/stack';
import React from "react";
import HomeScreen from "../screens/HomeScreen";
import AdViewScreen from "../screens/AdViewScreen";
import AdListScreen from "../screens/AdListScreen";
import AdCreateScreen from "../screens/AdCreateScreen";
import { StatusBar } from "expo-status-bar";

const Stack = createStackNavigator();

export default class Navigator extends React.Component {
    constructor (props) {
        super(props);
    }

    render () {
        return (
            <NavigationContainer>
                <StatusBar style="auto" />
                <Stack.Navigator initialRouteName="Home">
                    <Stack.Screen name="Home" component={HomeScreen} options={{ title: 'Covhelp' }} />
                    <Stack.Screen name="AdView" component={AdViewScreen} />
                    <Stack.Screen name="AdList" component={AdListScreen} options={{ title: 'Liste des annonces' }} />
                    <Stack.Screen name="AdCreate" component={AdCreateScreen} options={{ title: 'Créer une annonce' }} />
                </Stack.Navigator>
            </NavigationContainer>
            
        )
    }
}