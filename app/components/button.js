import React from "react";
import { View, Text, StyleSheet, TouchableOpacity } from 'react-native';

export default function FlatButton({text, onPress, disabled }) {
    let styles = disabled ? stylesDisabled : stylesEnabled
    return(
        <TouchableOpacity onPress={onPress} disabled={disabled}>
            <View style={styles.button}>
                <Text style={styles.buttonText}>
                    {text}
                </Text>
            </View>
        </TouchableOpacity>
    )

}
const stylesEnabled = StyleSheet.create({
    button: {
        borderRadius: 8,
        paddingVertical : 14,
        paddingHorizontal: 10,
        backgroundColor :'#24b2e0',
        marginTop: 0,
        margin: 12
    },
    buttonText: {
        color: 'white',
        fontWeight:'bold',
        fontSize: 16

    }
})

const stylesDisabled = StyleSheet.create({
    button: {
        borderRadius: 8,
        paddingVertical : 14,
        paddingHorizontal: 10,
        backgroundColor :'#24b2e0a0',
        marginTop: 0,
        margin: 12
    },
    buttonText: {
        color: 'white',
        fontWeight:'bold',
        fontSize: 16

    }
})